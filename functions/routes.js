var express = require('express');
var router = express.Router();


const { deleteProduct, createProduct, updateProduct, getOneProduct, getAllProducts } = require('./controllers')



router.post("/create", createProduct);

router.get("/read/:id", getOneProduct);

router.get("/read", getAllProducts);

router.put("/update/:id", updateProduct);

router.delete("/delete/:id", deleteProduct);

module.exports = router;
var admin = require("firebase-admin");
const db = admin.firestore();


const dbCollection = db.collection('products')



exports.createProduct = (req, res) => {
    (async() => {
        try {
            await db
                .collection("products")
                .doc("/" + req.body.id + "/")
                .create({
                    name: req.body.name,
                    description: req.body.description,
                    price: req.body.price,
                });
            return res.status(200).send();
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
}


exports.getOneProduct = (req, res) => {
    (async() => {
        try {
            const document = db.collection("products").doc(req.params.id);
            let product = await document.get();
            let response = product.data();
            return res.status(200).send(response);
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
}

exports.getAllProducts = (req, res) => {
    (async() => {
        try {
            const query = db.collection("products");
            let response = [];

            await query.get().then(querySnapshot => {
                let docs = querySnapshot.docs;

                for (let doc of docs) {
                    const selectedItem = {
                        id: doc.id,
                        name: doc.data().name,
                        description: doc.data().description,
                        price: doc.data().price
                    };
                    response.push(selectedItem);
                }
                return response;
            })
            return res.status(200).send(response);

        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
}


exports.updateProduct = (req, res) => {
    (async() => {
        try {
            const document = db.collection('products').doc(req.params.id);
            await document.update({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price,
            })
            return res.status(200).send();
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
}

exports.deleteProduct = (req, res) => {
    (async() => {
        try {
            const document = dbCollection.doc(req.params.id);
            await document.delete();
            return res.status(200).send();
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();
}
const functions = require("firebase-functions");

var admin = require("firebase-admin");
var serviceAccount = require("./permissions.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fir-crudapi-326c3.firebaseio.com",
});

const express = require("express");
const app = express();

const cors = require("cors");
const { query } = require("express");
app.use(cors({ origin: true }));

const myRouter = require('./routes')

//Routes
app.get("/helloworld", (req, res) => {
    return res.status(200).send("Hello World!");
});

app.use('/api', myRouter)



exports.app = functions.https.onRequest(app);